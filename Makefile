.PHONY: push

push:
	docker build -t tommd/haskell:latest .
	docker push tommd/haskell:latest
