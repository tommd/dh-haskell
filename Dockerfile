FROM ubuntu:18.04
LABEL maintainer "thomas.dubuisson@gmail.com"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && \
    apt install --yes curl \
                      gcc \
                      git \
                      libicu-dev libtinfo-dev libgmp-dev\
                      locales \
                      make \
                      python-dev python-pip python3-dev python3-pip \
                      software-properties-common \
                      sudo \
                      tzdata \
                      xz-utils \
                      zlib1g-dev \
                      nodejs npm \
                      zsh \
                      cmake libtool libtool-bin unzip m4 automake gettext
# RUN add-apt-repository ppa:neovim-ppa/stable && \
#     apt update && \
#     apt install --yes neovim
RUN git clone https://github.com/neovim/neovim /tmp/neovim && \
    cd /tmp/neovim && \
    make && make install
RUN update-alternatives --install /usr/bin/vi vi /usr/local/bin/nvim 60 && \
    update-alternatives --config vi && \
    update-alternatives --install /usr/bin/vim vim /usr/local/bin/nvim 60 && \
    update-alternatives --config vim && \
    update-alternatives --install /usr/bin/editor editor /usr/local/bin/nvim 60 && \
    update-alternatives --config editor
RUN adduser --disabled-password --gecos "" -q tommd --shell /usr/bin/bash
RUN echo "tommd           ALL = (ALL) NOPASSWD:ALL" >> /etc/sudoers
USER tommd
WORKDIR /home/tommd
RUN mkdir -p ~/.ghcup/bin && \
    curl -o ~/.ghcup/bin/ghcup https://raw.githubusercontent.com/haskell/ghcup/master/ghcup && \
    chmod +x ~/.ghcup/bin/ghcup
RUN ~/.ghcup/bin/ghcup install 8.6.5
RUN ~/.ghcup/bin/ghcup install-cabal
RUN $HOME/.ghcup/bin/ghcup set 8.6.5
RUN echo 'export PATH=$PATH:$HOME/.ghcup/bin:$HOME/.cabal/bin' >> $HOME/.bashrc
RUN echo 'export PATH=$PATH:$HOME/.ghcup/bin:$HOME/.cabal/bin' >> $HOME/.profile
RUN ~/.ghcup/bin/cabal v2-update && \
    export PATH=$PATH:$HOME/.ghcup/bin ; \
    PATH=$PATH ~/.ghcup/bin/cabal v2-install haskdogs hasktags \
                                             hlint apply-refact hindent stylish-haskell
RUN git clone https://github.com/haskell/haskell-ide-engine --recurse-submodules $HOME/hie
RUN cd $HOME/hie ; PATH=$PATH:$HOME/.ghcup/bin cabal v2-install hie
RUN curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
RUN git clone https://github.com/tommd/configuration $HOME/configuration && \
                 cd $HOME/configuration ; \
                 git checkout d9a91958aeb0e87ed5662f7f526903f895ac7bbe && \
                 bash $HOME/configuration/setup.sh
RUN sudo chsh -s /usr/bin/zsh tommd

# HaRe is bitrotted
# RUN su tommd -c 'git clone https://github.com/RefactoringTools/HaRe.git $HOME/hare'
# RUN su tommd -c 'cd $HOME/hare ; PATH=$PATH:$HOME/.ghcup/bin cabal v2-install ghc-hare'

# N.B. users can `usermod -l $whoami tommd`
CMD ["zsh"]
